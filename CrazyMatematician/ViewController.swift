//
//  ViewController.swift
//  CrazyMatematician
//
//  Created by Tolegen Mukhamedali on 5/20/19.
//  Copyright © 2019 Tolegen Mukhamedali. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    
    var bestScore = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bestScore = UserDefaults.standard.integer(forKey: "bestScore")
        self.scoreLabel.text = "\(bestScore)"
        
        // Do any additional setup after loading the view.
    }
    @IBAction func playButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toGameVC", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.destination.isKind(of: GameViewController.self)){
            let gameVC = segue.destination as! GameViewController
            gameVC.delegate = self
        }
        
    }
}

extension ViewController: GameViewControllerDelegate {
    
    func gameScore(score: Int) {
        
        if (score > bestScore) {
            bestScore = score
            UserDefaults.standard.set(bestScore, forKey: "bestScore")
        }
        
        self.scoreLabel.text = "\(bestScore)"
    }
    
}

