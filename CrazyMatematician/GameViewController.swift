//
//  GameViewController.swift
//  CrazyMatematician
//
//  Created by Tolegen Mukhamedali on 5/20/19.
//  Copyright © 2019 Tolegen Mukhamedali. All rights reserved.
//

import UIKit

protocol GameViewControllerDelegate {
    func gameScore(score: Int)
}

class GameViewController: UIViewController {
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var answerTextField: UITextField!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    var delegate: GameViewControllerDelegate?
    var x = 0
    var y = 0
    var gameTime = 30
    var score = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.timerLabel.text = "0:\(gameTime)"
        newQuestion()
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(GameViewController.updateTimer), userInfo: nil, repeats: true)
        
        _ = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(GameViewController.moveQuestionLabel), userInfo: nil, repeats: true)
        
        

        // Do any additional setup after loading the view.
    }
    
    func newQuestion(){
        self.x = Int(arc4random_uniform(9) + 1)
        self.y = Int(arc4random_uniform(9) + 1)
        self.questionLabel.text = "\(self.x)X\(self.y)="
        self.questionLabel.center.y = 50
    }
    
    @objc func moveQuestionLabel(){
        if (self.questionLabel.center.y >= self.bottomView.center.y){
            score -= 2
            scoreLabel.text = "Your score: \(score)"
            newQuestion()
        }
        UIView.animate(withDuration: 0.2) {
            self.questionLabel.center.y += 10
        }
    }
    
    @objc func updateTimer() {
        gameTime -= 1
        self.timerLabel.text = "0:\(gameTime)"
        if (gameTime == 0){
            gameOver()
        }
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        let res = Int(self.answerTextField.text!)
        
        if (res == x*y) {
            score += 1
            scoreLabel.text = "Your score: \(score)"
            newQuestion()
        }
        
        self.answerTextField.text = ""
        
    }
    
    func gameOver() {
        
        self.delegate?.gameScore(score: score)
        let alert = UIAlertController(title: "Game Over", message: "Your score is: \(score)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
